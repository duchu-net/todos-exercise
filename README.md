<h1 align="center">ToDo's</h1>
<p align="center">Simple example app.</p>
<p align="center">
  <img src="./docs/images/0.1.0-rc1.png" alt="screenshot">
</p>



### About
Functionalities:
- User can add todo items
- Todos can be edited
- Todos can be removed
- User can mark todos as “complete”
- User can see filtered todos: all, complete, incomplete
- User can search through todos

Extra functionality:
- Saving the data (ex. local storage) - made with `redux-persist` package
- Todos reordering - made with `sort` functionality
- Translations support - made with `react-intl` package, detect user locale
- created/updated ago - made with `moment` and updating interval

Tested on: 
- `Google Chrome 72.0.3626.121`
- `Mozilla Firefox 65.0.2`

`Who you gonna call? (bugbusters)` - If you found any error, please notify the author :)


### Dev Install
run command in bash:
```bash
npm install
```


### Dev Hot Reload Run
run command in bash:
```bash
npm run dev
```


### Build Run
run command in bash:
```bash
npm run build
```
After build app can be found in `./dist` folder


### Help?
- problem with environment, can't run/build? check `dist` branch for builded app in `./dist`
- in browser? try to delete cookies and [ctrl]+[F5]
- ask author


### ToDo's TODO's ;)
- `Dialog` title - don't provide default support for title as `React.Component` (`FormattedMessage` for translations)
- `TodosList` dates - connect `moment` with `react-intl` for locale dates
- `linter` - add support for development
- hashes in builded .js files (browser cache)
- `manifest.json`
