import React from 'react'
import PropTypes from 'prop-types'
// REDUX
import { Provider } from 'react-redux'
import createStore from './store/store'
const store = createStore()
// MATERIAL-UI
import { MuiThemeProvider, getMuiTheme } from 'material-ui/styles'
// LANGS
import LangProvider from './modules/langs/LangProvider'
// LAYOUT
import Layout from './components/layout/Layout'
// PAGES
import HomePage from './pages/HomePage'

class App extends React.Component {
  static childContextTypes = {
    muiTheme: PropTypes.object,
  }

  getChildContext() {
    return {
      muiTheme: this.muiTheme,
    }
  }

  constructor(props, context) {
    super(props, context)
    this.muiTheme = getMuiTheme()
  }

  render() {
    return (
      <Provider store={store}>
        <LangProvider>
          <MuiThemeProvider muiTheme={this.muiTheme}>
            <Layout>
              <HomePage />
            </Layout>
          </MuiThemeProvider>
        </LangProvider>
      </Provider>
    )
  }
}

export default App
