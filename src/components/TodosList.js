import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import moment from 'moment'
import { List, ListItem, Subheader, CardHeader, CardText, CardActions, Avatar } from 'material-ui'
import ActionDone from 'material-ui/svg-icons/action/done'
import ActionHourglass from 'material-ui/svg-icons/action/hourglass-empty'
import DateFromNow from './dates/DateFromNow'
import './TodosList.scss'

class TodosList extends React.Component {
  static propTypes = {
    renderActions: PropTypes.func.isRequired,
  }

  render() {
    const items = this.props.items

    return (
      <List className={'todos-list-component'}>
        {!items.length ? (
          <Subheader>
            <FormattedMessage id='main.nothing_to_show' defaultMessage='nothing to show' />
          </Subheader>
        ):(
          <Subheader>
            <FormattedMessage id='main.elements' defaultMessage='elements' />: {items.length}
          </Subheader>
        )}
        {items.map(item => {
          const createdAt = moment(item.created_at)
          const updatedAt = moment(item.updated_at)
          const isUpdated = Boolean(item.updated_at)
          
          const subtitle = (
            <div>
              <div>
                <FormattedMessage id='main.created' defaultMessage='created' /> <DateFromNow date={createdAt} /> ({createdAt.format('llll')}) 
              </div>
              {isUpdated && (
                <>
                  <div>
                    <FormattedMessage id='main.last_updated' defaultMessage='last updated' /> <DateFromNow date={updatedAt} /> ({updatedAt.format('llll')}) 
                  </div>
                  <div>
                    <FormattedMessage id='main.updates_count' defaultMessage='updates count' />: {item.updates_count} 
                  </div>
                </>
              )}
            </div>
          )

          return (
            <ListItem 
              style={{ cursor: 'inherit' }}
              key={`item_${item.id}`}
            >
              <CardHeader 
                avatar={<Avatar backgroundColor={item.completed ? 'lightgreen' : null}>
                  {item.completed ? <ActionDone /> : <ActionHourglass />}
                </Avatar>}
                subtitle={subtitle} 
              />
              <CardText>{item.text}</CardText>
              <CardActions style={{ textAlign: 'end' }}>{this.props.renderActions(item)}</CardActions>
            </ListItem>
          )
        })}
      </List>
    )
  }
}

export default TodosList
