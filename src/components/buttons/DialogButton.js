import React from 'react'
import { FlatButton, Dialog } from 'material-ui'

class DialogButton extends React.Component {
  state = {
    open: false,
  }

  handleOpen = () => {
    this.setState({ open: true });
  }

  handleClose = () => {
    this.setState({ open: false });
  }

  render() {
    return (
      <div>
        <FlatButton label={'open'} onClick={this.handleOpen} />
        <Dialog
          title="Dialog With Actions"
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
        >
          The actions in this window were passed in as an array of React objects.
          {this.props.renderBody && this.props.renderBody()}
          {this.props.children}
        </Dialog>
      </div>
    )
  }
}

export default DialogButton
