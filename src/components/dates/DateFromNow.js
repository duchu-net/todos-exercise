import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import moment from 'moment'

class DateFromNow extends React.Component {
  static propTypes = {
    date: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.instanceOf(Date),
      PropTypes.instanceOf(moment),
    ]).isRequired,
    interval: PropTypes.number,
    intl: PropTypes.object,
  }
  static defaultProps = {
    interval: 5000,
    intl: { locale: 'en' },
  }

  componentDidMount() {
    this.setInterval()
  }
  
  componentDidUpdate(prevProps) {
    if (this.props.interval !== prevProps.interval) {
      this.clearInterval()
      this.setInterval()
    }
  }
  componentWillUnmount() {
    this.clearInterval()
  }

  setInterval() {
    this._interval = setInterval(() => {
      this.forceUpdate()
    }, this.props.interval)
  }
  clearInterval() {
    if (this._interval) clearInterval(this._interval)
  }

  render() {
    return (
      <>{moment(this.props.date).locale(this.props.intl.locale).fromNow()}</>
    )
  }
}

export default injectIntl(DateFromNow)
