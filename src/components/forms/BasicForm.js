import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import { FlatButton, FontIcon } from 'material-ui'
import {
  CardActions,
  CardHeader,
  CardText
} from 'material-ui/Card'
import Form from './Form'

class BasicForm extends React.Component {
  static propTypes = {
    card: PropTypes.bool,
    title: PropTypes.any,
    button: PropTypes.bool,
    subtitle: PropTypes.any,
    onSubmit: PropTypes.func.isRequired,
    onSuccess: PropTypes.func,
  }
  static defaultProps = {
    card: true,
    title: null,
    button: true,
    subtitle: null,
    onSubmit: () => {},
  }
  state = {
    model: {},
    isValid: false,
    isSucced: null,
    response: null,
    inProgress: false,
    userMessageError: null,
  }

  getTitle() {
    return this.props.title
  }
  getSubtitle() {
    return this.props.subtitle
  }

  onSubmit() {
    const { isValid, model } = this.state
    if (!isValid) return null

    return Promise.resolve()
      .then(() => {
        this.setState({ inProgress: true, isSucced: null, userMessageError: null })
      })
      .then(() => this.props.onSubmit(model))
      .then(res => this.onSuccessSubmit(res))
      .catch(err => {
        // eslint-disable-next-line no-console
        console.warn('err:', err)
        const newState = { inProgress: false, isSucced: false }
        if (err.data && err.data.error) this.updateInputsWithError(err.data.error)
        if (err.userMessage) newState.userMessageError = err.userMessage
        this.setState(newState)
      })
  }

  onSuccessSubmit(res) {
    this.setState({ inProgress: false, isSucced: true, response: res.data })
    if (this.props.onSuccess) this.props.onSuccess()
  }

  onValid() {
    const { isValid } = this.state
    if (!isValid) this.setState({ isValid: true })
  }
  onInvalid() {
    const { isValid } = this.state
    if (isValid) this.setState({ isValid: false })
  }
  onChange(model) {
    this.setState({ model: model })
  }

  updateInputsWithError(err) {
    this.form.updateInputsWithError(err)
  }


  renderForm() {
    return (
      <div>to override</div>
    )
  }

  renderFormComponent() {
    return (
      <Form
        ref={ref => { this.form = ref }}
        onChange={(model) => this.onChange(model)}
        onValid={() => this.onValid()}
        onInvalid={() => this.onInvalid()}
      >
        {this.renderForm()}
      </Form>
    )
  }

  isValid() {
    return this.state.isValid
  }

  renderSubmitButton() {
    const { inProgress } = this.state
    return (
      <FlatButton
        type="submit"
        label={inProgress
          ? <FormattedMessage id='main.processing' defaultMessage='processing'/>
          : <FormattedMessage id='main.process' defaultMessage='process'/>
        }
        labelPosition="before"
        icon={inProgress
          ? <FontIcon className="fa fa-circle-notch fa-spin" />
          : <FontIcon className="fa fa-arrow-right" />
        }
        disabled={!this.isValid() || inProgress}
        onClick={() => this.onSubmit()}
      />
    )
  }

  renderNonFormErrors() {
    if (!this.state.userMessageError) return null
    return (
      <div className={'non_form_error'}>
        {this.state.userMessageError}
      </div>
    )
  }

  isInProgress() {
    return false
  }
  isDisabled() {
    return false
  }

  render() {
    const button = this.props.button
    const expandable = false
    const title = this.getTitle()
    const subtitle = this.getSubtitle()

    return (
      <div className={'basic_form'}>
        {(title || subtitle) && (
          <CardHeader
            title={title}
            subtitle={subtitle}
            className={'uppercase'}
            actAsExpander={expandable}
            showExpandableButton={expandable}
          />
        )}
        <CardText>
          {this.renderFormComponent()}
          {this.renderNonFormErrors()}
        </CardText>
        {button && (
          <CardActions style={{ textAlign: 'right', padding: '20px 0 0' }}>
            {this.renderSubmitButton()}
          </CardActions>
        )}
      </div>
    )
  }

}

export default BasicForm
