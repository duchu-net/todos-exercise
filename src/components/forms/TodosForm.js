import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Row, Col } from 'react-flexbox-grid'
import { RaisedButton, FontIcon } from 'material-ui'
import BasicForm from './BasicForm'
import TextField from './components/TextField'


class TodosForm extends BasicForm {
  renderSubmitButton() {
    const { inProgress } = this.state

    return (
      <RaisedButton
        type="submit"
        label={inProgress
          ? <FormattedMessage id='main.processing' defaultMessage='processing'/>
          : <FormattedMessage id='main.save' defaultMessage='save'/>
        }
        labelPosition="before"
        icon={inProgress
          ? <FontIcon className="fa fa-circle-notch fa-spin" />
          : <FontIcon className="fa fa-arrow-right" />
        }
        disabled={!this.isValid() || inProgress}
        onClick={() => this.onSubmit()}
        primary
      />
    )
  }


  renderForm() {
    const { isSucced, model = {} } = this.props
    const textIntl = <FormattedMessage id='main.text' defaultMessage='text'/>

    return (
      <Row>
        <Col xs={12}>
          <TextField
            name={'text'}
            hintText={textIntl}
            value={model.text}
            floatingLabelText={<span className={'capitalize'}>{textIntl}</span>}
            disabled={this.isDisabled()}
            validationError={<FormattedMessage id={'error.email_not_valid'} defaultMessage={'This is not a valid email'} />}
            fullWidth
            required
            multiLine
          />
        </Col>
        {isSucced && <Col xs={12}><FormattedMessage id='main.success' defaultMessage='success'/>...</Col>}
      </Row>
    )
  }
}

export default TodosForm
