import React from 'react'
import PropTypes from 'prop-types'
// Formsy.Decorator-https://github.com/christianalfoni/formsy-react/blob/master/API.md#ispristine
// import { Mixin, HOC } from 'formsy-react'

class FormElement extends React.Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
  }
  static defaultProps = {
    name: null,
    validationError: '',
    validationErrors: {},
  }
  isPristine() {
    return this.props.isPristine()
  }
  getValue() {
    return this.props.getValue()
  }
  setValue(value) {
    return this.props.setValue(value)
  }
  onChange(value) {
    return this.setValue(value)
  }
  getErrorMessage() {
    return this.props.getErrorMessage()
  }
  isDisabled() {
    return this.props.disabled
  }
  getLabel() {
    return this.props.label
  }
  showRequired() {
    return this.props.showRequired()
  }
  showError() {
    return this.props.showError()
  }

  render() {
    const className = ['form_element_component']
    if (this.showRequired()) className.push('required')
    if (this.showError()) className.push('error')

    return (
      <div className={className.join(' ')}>
        {this.getFormElementContent()}
      </div>
    )
  }

  getFloatingLabelText() {
    return this.props.floatingLabelText || 'Name'
  }

  getFormElementContent() {
    return <div>must be override</div>
  }
}

export default FormElement
