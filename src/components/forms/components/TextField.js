import React from 'react'
import TextFieldUI from 'material-ui/TextField'
import FormElement from './FormElement'

import { withFormsy } from 'formsy-react'


class TextField extends FormElement {
  onChange(event) {
    const value = event.currentTarget.value
    return this.setValue(value)
  }

  getTextFiledType() {
    if (this.props.hidden) return 'hidden'
    if (this.props.type) return this.props.type
    return 'text'
  }

  getInputValue() {
    return this.getValue() || ''
  }

  getFormElementContent() {
    const floatingLabelText = this.props.floatingLabelText

    // const validations = this.props.validations
    const maxLength = this.props.maxLength
    const minLength = this.props.minLength
    const hidden = this.props.hidden || false
    const display = hidden ? { display: 'none' } : {}
    const hintText = this.props.hintText
    const multiLine = this.props.multiLine
    const rows = multiLine ? (this.props.rows || 2) : 1
    const fullWidth = this.props.fullWidth

    return (
      <TextFieldUI
        value={this.getInputValue()}
        hintText={hintText}
        floatingLabelFixed
        floatingLabelText={floatingLabelText}
        errorText={this.getErrorMessage()}
        onChange={(...p) => this.onChange(...p)}
        // onKeyPress={(...p) => this.onKeyPress(...p)}
        style={{ width: '100%', ...display }}
        disabled={this.isDisabled()}
        maxLength={maxLength}
        minLength={minLength}
        max={maxLength}
        min={minLength}
        type={this.getTextFiledType()}
        multiLine={multiLine}
        rows={rows}
        rowsMax={5}
        fullWidth={fullWidth}
        // validations={validations}
      />
    )
  }
}


export default withFormsy(TextField)
