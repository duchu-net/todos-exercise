import React from 'react'
import { Grid } from 'react-flexbox-grid'
import { AppBar, CardText } from 'material-ui'
import './Layout.scss'
import SettingsButtonContainer from '../../containers/SettingsButtonContainer';

class Layout extends React.Component {
  render() {
    return (
      <div className={'layout-component'}>
        <div className={'layout-component-header'}>
          <AppBar
            title={'ToDo\'s'}
            showMenuIconButton={false}
            iconElementRight={<SettingsButtonContainer />}
          />
        </div>
        <div className={'layout-component-container'}>
          <div className={'layout-component-content'}>
            <Grid>
              {this.props.children}
            </Grid>
          </div>
          <div className={'layout-component-footer'}>
            <CardText>
              (v{__CONFIG.VERSION}) Created by Patryk Androsiuk (<a href='http://duchu.net' target='_blank'>duchu.net</a>)
            </CardText>
          </div>
        </div>
      </div>
    )
  }
}

export default Layout
