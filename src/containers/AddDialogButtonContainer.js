import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { FormattedMessage } from 'react-intl'
import { RaisedButton, Dialog } from 'material-ui'
import ActionAdd from 'material-ui/svg-icons/content/add'
import * as TodosActions from '../modules/todos/TodosActions'
import TodosForm from '../components/forms/TodosForm'

class AddDialogButtonContainer extends React.Component {
  static propTypes = {
    addTodo: PropTypes.func.isRequired,
    style: PropTypes.object,
  }
  static defaultProps = {
    style: {}
  }

  state = {
    open: false,
  }

  handleOpen = () => {
    this.setState({ open: true });
  }
  handleClose = () => {
    this.setState({ open: false });
  }

  render() {
    return (
      <div style={this.props.style}>
        <RaisedButton 
          label={<FormattedMessage id='main.add_new' defaultMessage='add new' />} 
          onClick={this.handleOpen} icon={<ActionAdd />} 
          primary 
        />
        <Dialog
          title={'New'}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
        >
          <TodosForm 
            ref={ref => { this._form = ref }}
            onSubmit={this.props.addTodo}
            onSuccess={this.handleClose}
          />
        </Dialog>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return { todos: state.Todos.todos }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(TodosActions, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(AddDialogButtonContainer)
