import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { FormattedMessage } from 'react-intl'
import { Dialog, IconButton } from 'material-ui'
import ActionEdit from 'material-ui/svg-icons/image/edit'
import * as TodosActions from '../modules/todos/TodosActions'
import TodosForm from '../components/forms/TodosForm'

class EditDialogButtonContainer extends React.Component {
  static propTypes = {
    addTodo: PropTypes.func.isRequired,
  }
  state = {
    open: false,
  }

  handleOpen = () => {
    this.setState({ open: true });
  }

  handleClose = () => {
    this.setState({ open: false });
  }

  render() {
    return (
      <span>
        <IconButton 
          tooltip={<FormattedMessage id='main.edit' defaultMessage='edit' />} 
          onClick={this.handleOpen}
        >
          <ActionEdit />
        </IconButton>
        <Dialog
          title={'Edit'}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
        >
          <TodosForm 
            ref={ref => { this._form = ref }}
            onSubmit={(model) => this.props.editTodo({ ...model, id: this.props.item.id })}
            onSuccess={this.handleClose}
            model={this.props.item}
          />
        </Dialog>
      </span>
    )
  }
}

function mapStateToProps(state) {
  return { todos: state.Todos.todos }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(TodosActions, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(EditDialogButtonContainer)
