import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { FormattedMessage } from 'react-intl'
import { FlatButton, Card, CardHeader, CardText, CardActions, TextField, RaisedButton } from 'material-ui'
import * as TodosActions from '../modules/todos/TodosActions'
import './FiltersContainer.scss'

class FiltersContainer extends React.Component {
  static propTypes = {
    addTodo: PropTypes.func.isRequired,

    filters: PropTypes.object,
  }
  state = {
    open: false,
  }

  handleOpen = () => {
    this.setState({ open: true });
  }

  handleClose = () => {
    this.setState({ open: false });
  }

  render() {
    const { filters } = this.props

    const renderItem = (label, actions) => (
      <div className={'filters-component-item'} key={`item_${label}`}>
        <div className={'filters-component-item-label'}>
          <FormattedMessage id={`main.${label}`} defaultMessage={label} />
          {/* {label} */}
        </div>
        <div className={'filters-component-item-actions'}>{actions}</div>
      </div>
    )

    const items = {
      text: (
        <div className={'item-text'}>
          <div>
            <TextField 
              name={'query'}
              hintText={<FormattedMessage id='main.text' defaultMessage='text'/>}
              onChange={event => {
                if (!event.target.value) this.props.setFilter({ text: null })
                else this.props.setFilter({ text: [['$match', event.target.value]] })
              }} 
              value={Array.isArray(filters.text) && filters.text.length ? filters.text[0][1] : ''}
              fullWidth
            />
          </div>
          <div>
            <FlatButton 
              label={<FormattedMessage id='main.clear' defaultMessage='clear'/>} 
              onClick={() => this.props.setFilter({ text: null })} 
              disabled={!Array.isArray(filters.text)}
            />
          </div>
        </div>
      ),
      completed: (
        <>
          <RaisedButton 
            label={<FormattedMessage id='main.show_all' defaultMessage='show all'/>} 
            onClick={() => this.props.setFilter({ completed: null })} 
            disabled={!Array.isArray(filters.completed)}
          />
          <RaisedButton 
            label={<FormattedMessage id='main.completed' defaultMessage='completed'/>} 
            onClick={() => this.props.setFilter({ completed: [['$eq', true]] })} 
            disabled={Array.isArray(filters.completed) && filters.completed[0][1] == true}
          />
          <RaisedButton 
            label={<FormattedMessage id='main.incompleted' defaultMessage='incompleted'/>}  
            onClick={() => this.props.setFilter({ completed: [['$eq', false]] })} 
            disabled={Array.isArray(filters.completed) && filters.completed[0][1] == false}
          />
        </>
      )
    }

    const countActiveFilters = Object.values(filters).filter(item => item != null).length

    return (
      <Card className={'filters-component'}>
        <CardHeader 
          title={<FormattedMessage id='main.filters' defaultMessage='filters'/>} 
          subtitle={<><FormattedMessage id='main.active_filters' defaultMessage='active filters'/>: {countActiveFilters}</>}
          actAsExpander
          showExpandableButton
        />
        <CardText expandable>
          {Object.entries(items).map(([field, render]) => renderItem(field, render))}
        </CardText>
        <CardActions style={{ textAlign: 'end' }}>
          <FlatButton 
            label={<FormattedMessage id='main.clear_all_filters' defaultMessage='clear all filters'/>} 
            onClick={() => this.props.setFilter(null)}
            disabled={!countActiveFilters} 
          />
        </CardActions>
      </Card>
    )
  }
}

function mapStateToProps(state) {
  return { filters: state.Todos.filters }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(TodosActions, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(FiltersContainer)
