import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { FormattedMessage } from 'react-intl'
import { FlatButton, Dialog, IconButton } from 'material-ui'
import ActionSettings from 'material-ui/svg-icons/action/settings'
import * as TodosActions from '../modules/todos/TodosActions'
import SwitchLocale from '../modules/langs/SwitchLocale'

class SettingsButtonContainer extends React.Component {
  static propTypes = {
    addTodo: PropTypes.func.isRequired,
  }
  state = {
    open: false,
  }

  handleOpen = () => {
    this.setState({ open: true });
  }

  handleClose = () => {
    this.setState({ open: false });
  }

  render() {
    const actions = [
      <FlatButton
        label='ok'
        primary={true}
        onClick={this.handleClose}
      />,
    ]

    return (
      <div>
        <IconButton 
          tooltip={<FormattedMessage id='main.settings' defaultMessage='settings'/>} 
          onClick={this.handleOpen}
        >
          <ActionSettings />
        </IconButton>
        <Dialog
          title={'Settings'}
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
        >
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <div style={{ minWidth: 150 }}>
              <FormattedMessage id='main.language' defaultMessage='language'/>
            </div>
            <SwitchLocale />
          </div>
        </Dialog>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {}
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(TodosActions, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(SettingsButtonContainer)
