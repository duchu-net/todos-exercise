import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as TodosActions from '../modules/todos/TodosActions'
import TodosForm from '../components/forms/TodosForm'


class TodosFormContainer extends React.Component {
  static propTypes = {
    todos: PropTypes.array,
    addTodo: PropTypes.func,
  }
  render() {
    return <TodosForm items={this.props.todos} onSubmit={this.props.addTodo} />
  }
}

function mapStateToProps(state) {
  return { todos: state.Todos.todos }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(TodosActions, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(TodosFormContainer)
