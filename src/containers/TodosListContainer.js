import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { FormattedMessage } from 'react-intl'
import { IconButton } from 'material-ui'
import ActionDelete from 'material-ui/svg-icons/action/delete'
import ActionDone from 'material-ui/svg-icons/action/done'
import ActionClear from 'material-ui/svg-icons/content/clear'

import * as TodosActions from '../modules/todos/TodosActions'
import TodosList from '../components/TodosList'
import EditDialogButtonContainer from './EditDialogButtonContainer'


class TodosListContainer extends React.Component {
  static propTypes = {
    todos: PropTypes.array.isRequired,
    deleteTodo: PropTypes.func.isRequired,
    toggleCompleteTodo: PropTypes.func.isRequired,
  }
  
  render() {
    return (
      <TodosList 
        items={this.props.todos}
        renderActions={item => {
          return (
            <div>
              {item.completed ? (
                <IconButton 
                  tooltip={<FormattedMessage id='main.incompleted' defaultMessage='incompleted' />} 
                  onClick={() => this.props.toggleCompleteTodo(item)}
                >
                  <ActionClear />
                </IconButton>
              ):(
                <IconButton 
                  tooltip={<FormattedMessage id='main.completed' defaultMessage='completed' />} 
                  onClick={() => this.props.toggleCompleteTodo(item)}
                >
                  <ActionDone />
                </IconButton>
              )}

              <EditDialogButtonContainer item={item} />

              <IconButton 
                tooltip={<FormattedMessage id='main.delete' defaultMessage='delete' />} 
                onClick={() => this.props.deleteTodo(item)}
              >
                <ActionDelete />
              </IconButton>
            </div>
          )
        }} 
      />
    )
  }
}


function todosFilter(todos = [], filters = {}) {
  return todos.filter(item => {
    for (const [field, rules] of Object.entries(filters)) {
      if (!Array.isArray(rules)) continue
      for (const [rule, value] of rules) {
        switch (rule) {
          case '$eq': {
            if (item[field] !== value) return false
          }
          case '$match': {
            if (String(item[field]).toLowerCase().indexOf(String(value).toLowerCase()) === -1) return false
          }
        }
      }
    }
    return true
  })
}

function todosSort(todos = [], rule) {
  if (!rule || !Array.isArray(rule) || !rule.length) return todos
  // 1 ascending, -1 descending
  function sortByDate(field = 'created_at', mode = 1, a, b) {
    if (!a[field] && !b[field]) return 0
    if (a[field] && !b[field]) return -1
    if (!a[field] && b[field]) return 1
    
    const aD = new Date(a[field])
    const bD = new Date(b[field])
    if (mode === 1) return aD - bD
    if (mode === -1) return bD - aD
    return 0
  }
  return todos.sort(sortByDate.bind(this, rule[0], rule[1]))
}

function mapStateToProps(state) {
  return { 
    todosAll: state.Todos.todos,
    todos: todosSort(todosFilter(state.Todos.todos, state.Todos.filters), state.Todos.sort)
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(TodosActions, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(TodosListContainer)
