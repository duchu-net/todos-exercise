import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { FormattedMessage } from 'react-intl'
import { SelectField, MenuItem } from 'material-ui'

import AddDialogButtonContainer from './AddDialogButtonContainer'
import * as TodosActions from '../modules/todos/TodosActions'
import './TodosListHeaderContainer.scss'


class TodosListHeaderContainer extends React.Component {
  static propTypes = {
    setSort: PropTypes.func.isRequired,
    sort: PropTypes.array.isRequired,
  }

  handleChange = (event, index, value) => {
    const rule = value.split('.')
    this.props.setSort([rule[0], parseInt(rule[1])])
  }
  
  render() {
    const { sort } = this.props
    const items = [
      { value: 'created_at.-1', text: <FormattedMessage id='main.created_at_desc' defaultMessage='created at - descending' /> },
      { value: 'created_at.1', text: <FormattedMessage id='main.created_at_asc' defaultMessage='created at - ascending' /> },
      { value: 'updated_at.-1', text: <FormattedMessage id='main.updated_at_desc' defaultMessage='updated at - descending' /> },
      { value: 'updated_at.1', text: <FormattedMessage id='main.updated_at_asc' defaultMessage='updated at - ascending' /> },
    ]

    const style = { margin: 5 }

    return (
      <div className={'todos-list-header-component'}>
        <AddDialogButtonContainer style={style} />
        <SelectField
          floatingLabelText={<FormattedMessage id='main.sort' defaultMessage='sort' />}
          style={style}
          value={sort.join('.')}
          onChange={this.handleChange}
          maxHeight={200}
        >
          {items.map((item, index) => ( 
            <MenuItem 
              value={item.value} 
              primaryText={item.text} 
              key={`sort_${index}`} 
            />
          ))}
        </SelectField>
      </div>
    )
  }
}


function mapStateToProps(state) {
  return { 
    sort: state.Todos.sort,
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(TodosActions, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(TodosListHeaderContainer)
