import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
// LOCALIZATION
import { addLocaleData, IntlProvider } from 'react-intl'
import enLocaleData from 'react-intl/locale-data/en'
import plLocaleData from 'react-intl/locale-data/pl'
addLocaleData([...enLocaleData, ...plLocaleData])
// ACTIONS
import * as LangsActions from './LangsActions'

const messages = {
  en: {
    // Fallback to component props
  },
  pl: {
    'main.save': 'zapisz',
    'main.language': 'język',
    'main.text': 'treść',
    'main.process': 'przetwarzaj',
    'main.processing': 'przetwarzanie',
    'main.switch_to_window': 'przełącz do okna',
    'main.switch_to_fullscreen': 'przełącz do pełnego ekranu',
    'main.message': 'wiadomość',
    'main.settings': 'ustawienia',
    'main.filters': 'filtry',
    'main.active_filters': 'aktywne filtry',
    'main.clear_all_filters': 'wyczyść wszystkie filtry',
    'main.clear': 'wyczyść',
    'main.show_all': 'pokaż wszystkie',
    'main.completed': 'ukończone',
    'main.incompleted': 'nieukończone',
    'main.add_new': 'dodaj nowy',
    'main.edit': 'edycja',
    'main.delete': 'usuń',
    'main.sort': 'sortowanie',
    'main.created_at_desc': 'utworzono - malejąco',
    'main.created_at_asc': 'utworzono - rosnąco',
    'main.updated_at_desc': 'zaktualizowano - malejąco',
    'main.updated_at_asc': 'zaktualizowano - rosnąco',
    'main.nothing_to_show': 'nic do pokazania',
    'main.elements': 'elementy',
    'main.created': 'utworzono',
    'main.last_updated': 'ostatnio aktualizowano',
    'main.updates_count': 'liczba aktualizacji',
  }
}


class LangProvider extends React.PureComponent {
  static propTypes = {
    locale: PropTypes.string,
    default: PropTypes.string,
    children: PropTypes.any.isRequired,
    switchLocale: PropTypes.func.isRequired,
  }
  static defaultProps = {
    default: 'en',
  }


  constructor(props, context) {
    super(props, context)

    if (props.locale == null) {
      // example: pl-PL, en-EN
      const locale = props.locale || ['pl', 'en'].find(key => key == navigator.language.split(/[-_]/)[0]) 
        ? navigator.language.split(/[-_]/)[0] 
        : props.default
      props.switchLocale(locale)
    }
  }


  static childContextTypes = {
    setLocale: PropTypes.func,
  }
  getChildContext() {
    return {
      setLocale: this.setLocale,
    }
  }
  setLocale = (locale) => {
    // eslint-disable-next-line no-console
    if (!['pl', 'en'].find(key => key == locale)) return console.warn(`${locale} locale not found`)
    this.props.switchLocale(locale)
  }


  render() {
    const locale = this.props.locale || this.props.default
    return (
      <IntlProvider locale={locale} messages={messages[locale]}>
        {this.props.children}
      </IntlProvider>
    )
  }
}


function mapStateToProps(state) {
  return { locale: state.Langs && state.Langs.locale }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(LangsActions, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(LangProvider)

