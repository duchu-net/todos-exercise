export const LANGS = {
  SWITCH_LOCALE: 'langs/SWITCH_LOCALE',
}

export function switchLocale(locale) {
  return {
    type: LANGS.SWITCH_LOCALE,
    payload: locale
  }
}
