import update from 'immutability-helper'
import { LANGS } from './LangsActions'

const initialState = {
  locale: null,
}

export default function Langs(state = initialState, action) {
  switch (action.type) {
    case LANGS.SWITCH_LOCALE: return update(state, { locale: { $set: action.payload } })
    default: return state
  }
}
