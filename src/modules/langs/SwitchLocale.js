import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl } from 'react-intl'
import { MenuItem, IconMenu, IconButton } from 'material-ui'

import flagPl from './flags/locale-pl.png'
import flagEn from './flags/locale-en.png'
import './SwitchLocale.scss'


class SwitchLocale extends React.Component {
  static contextTypes = {
    setLocale: PropTypes.func,
  }
  static propTypes = {
    intl: PropTypes.object.isRequired,
  }

  constructor(props, context) {
    super(props, context)
    this.state = {}
    this.items = [
      {
        locale: 'en',
        name: 'english',
        flag: flagEn,
      },
      {
        locale: 'pl',
        name: 'polish',
        flag: flagPl,
      }
    ]
  }

  

  render() {
    return (
      <IconMenu
        iconButtonElement={(
          <IconButton 
            className={'current_flag'}
            tooltipPosition={'top-left'}
          >
            <img src={this.props.intl.locale == 'pl' ? flagPl : flagEn} />
          </IconButton>
        )}
        anchorOrigin={{horizontal: 'left', vertical: 'top'}}
        targetOrigin={{horizontal: 'left', vertical: 'bottom'}}
      >
        {this.items.map(item => (
          <MenuItem 
            key={item.locale} 
            value={item.locale} 
            primaryText={item.name} 
            onClick={() => this.context.setLocale(item.locale)}
            insetChildren={true}
            checked={this.props.intl.locale == item.locale}
            rightIcon={<img src={item.flag} />}
          />
        ))}
      </IconMenu>
    )
  }
}

export default injectIntl(SwitchLocale)
