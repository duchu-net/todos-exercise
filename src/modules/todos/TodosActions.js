export const TODOS = {
  ADD_TODOS: 'todos/ADD',
  EDIT_TODOS: 'todos/EDIT',
  DELETE_TODOS: 'todos/DELETE',
  // COMPLETE_TODOS: 'todos/COMPLETE',
  TOGGLE_COMPLETE_TODOS: 'todos/TOGGLE_COMPLETE',
  SET_FILTER: 'todos/SET_FILTER',
  CLEAR_FILTERS: 'todos/CLEAR_FILTERS',
  SET_SORT: 'todos/SET_SORT',
  CLEAR_SORT: 'todos/CLEAR_SORT',
}

export function addTodo(model) {
  return {
    type: TODOS.ADD_TODOS,
    payload: model
  }
}
export function editTodo(model) {
  return (dispatch, getState) => {
    if (model.id == null) throw { id: 'not set' }
    return dispatch({
      type: TODOS.EDIT_TODOS,
      payload: model
    })
  }
}
export function deleteTodo(model) {
  return (dispatch, getState) => {
    if (model.id == null) throw { id: 'not set' }
    return dispatch({
      type: TODOS.DELETE_TODOS,
      payload: model.id
    })
  } 
}
export function toggleCompleteTodo(model) {
  return (dispatch, getState) => {
    if (model.id == null) throw { id: 'not set' }
    return dispatch({
      type: TODOS.TOGGLE_COMPLETE_TODOS,
      payload: model.id
    })
  } 
}

export function setFilter(model) {
  if (model == null) {
    return {
      type: TODOS.CLEAR_FILTERS,
    }
  }
  return {
    type: TODOS.SET_FILTER,
    payload: model,
  }
}

export function setSort(model) {
  if (model == null) {
    return {
      type: TODOS.CLEAR_SORT,
    }
  }
  return {
    type: TODOS.SET_SORT,
    payload: model,
  }
}
