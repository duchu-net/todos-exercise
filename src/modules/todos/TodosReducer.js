import update from 'immutability-helper'
import { TODOS } from './TodosActions'

const initialState = {
  todos: [],
  // MUST BE CHANGED FOR IMPORT/EXPORT FUNCTIONALITY 
  last_id: -1,
  // FILTERS
  filters: {
    // ENTRY EXAMPLE:
    // DOUBLE ARRAY FOR FUTURE FUNCTIONALITY (multiple filters for one field)
    // completed: [['$eq', true]],
  },
  sort: ['created_at', -1],
}

export default function Todos(state = initialState, action) {
  switch (action.type) {
    case TODOS.ADD_TODOS: return update(state, {
      todos: { $push: [{ 
        ...action.payload, 
        id: state.last_id + 1,
        created_at: new Date().toJSON(),
        completed: false,
      }] },
      last_id: { $set: state.last_id + 1 }
    })

    case TODOS.EDIT_TODOS: {
      const { id, ...model } = action.payload
      const index = state.todos.findIndex(item => item.id === id)
      return update(state, {
        todos: {
          [index]: { $set: { 
            ...state.todos[index], 
            ...model,
            // UPDATES COUNT 
            updates_count: state.todos[index].updates_count == null ? 1 : state.todos[index].updates_count + 1,
            // LAST UPDATE TIME
            updated_at: new Date().toJSON()
          } }
        }
      })
    }

    case TODOS.TOGGLE_COMPLETE_TODOS: {
      const id = action.payload
      const index = state.todos.findIndex(item => item.id === id)
      return update(state, {
        todos: {
          [index]: {
            $toggle: ['completed'],
            completed_at: { $set: new Date().toJSON() },
          }
        }
      })
    }

    case TODOS.DELETE_TODOS: return update(state, {
      todos: { $splice: [[state.todos.findIndex(item => item.id === action.payload), 1]] }
    })

    case TODOS.SET_FILTER: return update(state, {
      // WE CAN DELETE IF SOME KEYS VALUE == null (for dynamic keys), BUT $merge IS ENOUGHT
      filters: { $merge: action.payload },
    })
    case TODOS.CLEAR_FILTERS: return update(state, {
      filters: { $set: {} },
    })
    
    case TODOS.SET_SORT: return update(state, {
      sort: { $merge: action.payload },
    })
    case TODOS.CLEAR_SORT: return update(state, {
      sort: { $set: initialState.sort },
    })
    
    default: return state
  }
}
