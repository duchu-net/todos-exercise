import React from 'react'

import { Paper, Divider } from 'material-ui'
import TodosListContainer from '../containers/TodosListContainer'
import TodosListHeaderContainer from '../containers/TodosListHeaderContainer'
import FiltersContainer from '../containers/FiltersContainer'

class HomePage extends React.Component {
  render() {
    return (
      <>
        <FiltersContainer />
        <Divider style={{ margin: '5px 0' }} />
        <Paper>
          <TodosListHeaderContainer />
          <TodosListContainer />
        </Paper>
      </>
    )
  }
}

export default HomePage
