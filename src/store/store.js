import { createStore as cs, applyMiddleware, combineReducers, compose } from 'redux'
import thunk from 'redux-thunk'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

// DEFAULT REDUCERS
import Todos from '../modules/todos/TodosReducer'
import Langs from '../modules/langs/LangsReducer'

export default function createStore() {
  let store = null
  const middlewares = [thunk]

  if (process.env.NODE_ENV === 'development') {
    // STORE LOGGER
    // eslint-disable-next-line no-undef
    const { createLogger } = require('redux-logger')
    const logger = createLogger({
      level: 'info',
      collapsed: true,
    })
    middlewares.push(logger)
  }

  store = cs(createReducer(), compose(
    applyMiddleware(...middlewares)
  ))
  store.persistor = persistStore(store)

  return store
}


function createReducer (reducers = {}) {
  return persistReducer({
    key: 'root',
    storage,
    // There is an issue in the source code of redux-persist (default setTimeout does not cleaning)
    timeout: null, 
  }, combineReducers({
    Langs,
    Todos,
    ...reducers
  }))
}
