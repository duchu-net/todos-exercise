import path from 'path'
import webpack from 'webpack'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import CopyWebpackPlugin from 'copy-webpack-plugin'
import pkg from '../package.json'

/* eslint-disable no-undef */
const DIRNAME = __dirname
const ENV = process.env
/* eslint-enable no-undef */
const NODE_ENV = ENV.NODE_ENV || 'production'

function setupConfig(config = {}) {
  Object.assign(config, {
    VERSION: JSON.stringify(pkg.version),
    NODE_ENV: JSON.stringify(NODE_ENV),
  })
  return config
}
const CONFIG = setupConfig()


const SHOW_LOG = true
if (SHOW_LOG) {
  /* eslint-disable */
  const colorConLog = s => console.log('\x1b[32m%s\x1b[0m', s)
  console.log('')
  colorConLog('----------------- BUILD OVERRIDE DEFAULT CONFIG: ------------------')
  console.log(JSON.stringify(CONFIG, null, 2))
  colorConLog('-------------------------------------------------------------------')
  console.log('')
  /* eslint-enable */
}


export default {
  target: 'web',
  mode: NODE_ENV,
  entry: [],
  output: {
    path: path.resolve(DIRNAME, '../dist'),
    filename: '[name].bundle.js',
    chunkFilename: '[name].chunk.js',
  },
  resolve: {
    alias: {
      modules: path.resolve(DIRNAME, '../src/modules'),
      containers: path.resolve(DIRNAME, '../src/containers'),
      components: path.resolve(DIRNAME, '../src/components'),
      pages: path.resolve(DIRNAME, '../src/pages'),
      assets: path.resolve(DIRNAME, '../src/assets'),
    },
    modules: ['node_modules'],
  },
  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendor',
          chunks: 'all'
        }
      }
    }
  },
  module: {
    rules: [
      // IMAGE FILES
      {
        test: /\.(jpe?g|ttf|eot|svg|png|gif|vash)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader?name=./assets/[name].[hash].[ext]'
      },
      // END IMAGE FILES

      // STYLES
      {
        test: /\.scss$/,
        loader: 'style-loader!css-loader!sass-loader'
      },
      {
        test: /\.less$/,
        loader: 'style-loader!css-loader!less-loader'
      },
      {
        test: /\.global\.css$/,
        loaders: [
          'style-loader',
          'css-loader?sourceMap'
        ]
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader?modules',
        include: /flexboxgrid/
      },
      {
        test: /^((?!\.global).)*\.css$/,
        loaders: [
          'style-loader',
          'css-loader?modules&sourceMap&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]'
        ],
        exclude: /flexboxgrid/
      },
      // END STYLES
    ]
  },
  plugins: [
    // ENVIRONMENT
    new webpack.DefinePlugin({
      __CONFIG: CONFIG,
    }),
    // TEMPLATE ENGINE
    new HtmlWebpackPlugin({
      template: path.resolve(DIRNAME, '../src/index.html'),
    }),
    // COPY FILES
    new CopyWebpackPlugin([
      { from: 'src/favicon.ico' },
      { from: 'src/robots.txt' },
    ])
  ]
}
