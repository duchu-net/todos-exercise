import path from 'path'
import baseConfig from './webpack.config.base'

export default {
  ...baseConfig,
  devtool: 'inline-source-map',
  devServer: {
    contentBase: path.resolve(__dirname, '../dist'),
  },
  entry: [
    ...baseConfig.entry,
    'webpack/hot/only-dev-server',
    'webpack-dev-server/client?http://localhost:8080',
    path.resolve(__dirname, '../src/index.js'),
  ],
  module: {
    ...baseConfig.module,
    rules: [
      {
        test: /\.js?$/,
        loaders: ['react-hot-loader/webpack', 'babel-loader'],
        exclude: /node_modules/
      },
      ...baseConfig.module.rules,
    ]
  },
}
