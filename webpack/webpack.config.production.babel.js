import path from 'path'
import baseConfig from './webpack.config.base'

export default {
  ...baseConfig,
  entry: [
    path.resolve(__dirname, '../src/index.js'),
  ],
  module: {
    ...baseConfig.module,
    rules: [
      {
        test: /\.js?$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      ...baseConfig.module.rules,
    ]
  },
}
